# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :pow_auth,
  ecto_repos: [PowAuth.Repo]

# Configures the endpoint
config :pow_auth, PowAuthWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "4ZvmUknYl3l0yblVXYI9hpMYIf32iIU7xVZj8nEdfvYzAvg+/v/A7ukNP2h7/PVh",
  render_errors: [view: PowAuthWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PowAuth.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :pow_auth, :pow,
  user: PowAuth.Users.User,
  repo: PowAuth.Repo,
  web_module: PowAuthWeb,
  extensions: [PowResetPassword, PowEmailConfirmation],
  controller_callback: Pow.Extension.Phoenix.ControllerCallbacks,
  mailer_backend: PowAuth.PowMailer,
  web_mailer_module: PowAuthWeb,
  routes_backend: PowAuthWeb.Pow.Routes
  # Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
