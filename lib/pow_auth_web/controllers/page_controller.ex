defmodule PowAuthWeb.PageController do
  use PowAuthWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
