defmodule PowAuthWeb.PowEmailConfirmation.MailerView do
  use PowAuthWeb, :mailer_view

  def subject(:email_confirmation, _assigns), do: "Confirm your email address"
end
