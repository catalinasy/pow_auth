defmodule PowAuth.Repo do
  use Ecto.Repo,
    otp_app: :pow_auth,
    adapter: Ecto.Adapters.Postgres
end
